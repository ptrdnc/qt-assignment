# qt-assignment

A simple user editor.

After cloning the project, enter the project directory

```
mkdir build
cd build
cmake -g "Unix Makefiles" -DCMAKE_PREFIX_PATH="/usr/lib64/cmake" ..
make MerlinMonro
```
