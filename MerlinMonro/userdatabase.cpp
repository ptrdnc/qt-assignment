#include "userdatabase.h"
#include "user.h"
#include "mainwindow.h"

#include <QFile>
#include <QJsonDocument>
#include <QVariant>



UserDatabase::UserDatabase(QWidget* parent)
{
    this->parent_ = parent;
}

UserDatabase::~UserDatabase()
{
    for (auto &u : users_) {
        delete u;
    }
    users_.clear();
}
// adds user to the database
void UserDatabase::addUser(User* user)
{
    users_.append(user);

    MainWindow* mw = (MainWindow*) parent_;
    mw->addUserToListWidget(user);

}
// removes user from the database
void UserDatabase::removeUser(User *user)
{
    users_.removeAt(users_.indexOf(user));
    delete user;

    MainWindow* mw = (MainWindow*) parent_;
    mw->removeUserFromListWidget();
}
// loads users from the database
void UserDatabase::loadUsers()
{

    QFile file(filePath_);
    file.open(QFile::ReadOnly);

    QJsonDocument doc = QJsonDocument::fromJson(file.readAll());

    QVariantList docVariant = doc.toVariant().toList();

    for (auto &x : docVariant) {
        User* user = new User();
        user->fromQVariant(x);

        users_.append(user);
    }

    file.close();
}
// saves users to the database
void UserDatabase::saveUsers()
{
    QVariantList list;
    for (auto& u : users_)
        list.append(u->toQVariant());

    QJsonDocument doc = QJsonDocument::fromVariant(list);
    QFile file(filePath_);
    file.open(QFile::WriteOnly);
    file.write(doc.toJson(QJsonDocument::JsonFormat::Indented));

    file.close();
}

