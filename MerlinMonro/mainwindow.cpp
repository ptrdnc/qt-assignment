#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QMessageBox>
#include <QPushButton>
#include <QShortcut>

#include <map>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    userDatabase = new UserDatabase(this);
    userForm = new UserForm(userDatabase);

    showBlankUserDetails();
    load();
}
MainWindow::~MainWindow()
{
    delete ui;
}
// Loads listwidget with data from user database (json)
void MainWindow::load() {

    userDatabase->loadUsers();

    connect(ui->lwUsers, &QListWidget::itemClicked, this, &MainWindow::lwItemClicked);

    for (auto &u : userDatabase->users_) {
        addUserToListWidget(u);
    }
}
// Fills the right side (user description) with blank labels.
// Aesthetic purposes.
void MainWindow::showBlankUserDetails() {

    while(ui->flUserDetails->rowCount() > 0) {
        ui->flUserDetails->removeRow(0);
    }

    for (int i = 0; i < 10; i++)
        ui->flUserDetails->addRow(new QLabel("\t\t\t\t"), new QLabel("\t\t\t\t"));

}
// Adds user data to listWidget on the left side.
void MainWindow::addUserToListWidget(User *user)
{
    ui->lwUsers->addItem(user->toQString());
    int nUsers = ui->lwUsers->count() - 1;
    auto lastItem = ui->lwUsers->item(nUsers);

    itemUserMap[lastItem] = user;
}
// Removes  the removed user from the list widget
void MainWindow::removeUserFromListWidget()
{
    if (selectedItem == nullptr)
        return;
//    I may use this in the future. let it exist as a comment...
//    QListWidgetItem* it = ui->lwUsers->takeItem(ui->lwUsers->currentRow());
//    itemUserMap.erase(it);
    itemUserMap.erase(selectedItem);
    delete selectedItem;
    selectedItem = nullptr;
}
// selects the item so we can show it's data on the right side
void MainWindow::lwItemClicked(QListWidgetItem(*item)) {

    selectedItem = item;
    showUserOnItem(item);
}
// shows data of the user on the clicked item
void MainWindow::showUserOnItem(QListWidgetItem *item) {

    while(ui->flUserDetails->rowCount() > 0) {
        ui->flUserDetails->removeRow(0);
    }

    User* user = itemUserMap[item];

    ui->flUserDetails->addRow(new QLabel("First name:"), new QLabel(user->firstName_));
    ui->flUserDetails->addRow(new QLabel("Last name:"), new QLabel(user->lastName_));
    ui->flUserDetails->addRow(new QLabel("Gender:"), new QLabel(user->gender_));
    ui->flUserDetails->addRow(new QLabel("Date of birth:"), new QLabel(user->dateOfBirth_));
    ui->flUserDetails->addRow(new QLabel("Address:"), new QLabel(user->streetAddress_));
    ui->flUserDetails->addRow(new QLabel("City:"), new QLabel(user->city_));
    ui->flUserDetails->addRow(new QLabel("State:"), new QLabel(user->state_));
    ui->flUserDetails->addRow(new QLabel("Postal code:"), new QLabel(user->postalCode_));

    QListWidget* lwPhone = new QListWidget();

    lwPhone->addItem("home:" + user->homeNumber_);
    lwPhone->addItem("mobile:" + user->mobileNumber_);

    ui->flUserDetails->addRow(new QLabel("Phone:"), lwPhone);

}
void MainWindow::closeEvent(QCloseEvent *event) {

    userDatabase->saveUsers();
    delete userForm;

}
void MainWindow::on_pbAdd_clicked()
{
    userForm->show();
}
void MainWindow::on_pbRemove_clicked()
{

    User* user = itemUserMap[selectedItem];
    if (user == nullptr)
        return;

    QMessageBox* box = new QMessageBox(QMessageBox::Icon::Question, "Delete user",
                                       "Are you sure you want to delete " + user->toQString() + "?");
    connect(box, &QMessageBox::accepted, this, &MainWindow::onRemoveAccept);
    QPushButton* noButton = new QPushButton("&No");
    QShortcut* noShortcut = new QShortcut(QKeySequence("n"), noButton);
    connect(noShortcut, &QShortcut::activated, noButton, &QPushButton::click);
    box->addButton(noButton, QMessageBox::RejectRole);

    QPushButton* yesButton = new QPushButton("&Yes");
    QShortcut* yesShortcut = new QShortcut(QKeySequence("y"), yesButton);
    connect(yesShortcut, &QShortcut::activated, yesButton, &QPushButton::click);
    box->addButton(yesButton, QMessageBox::AcceptRole);

    connect(noShortcut, &QShortcut::activated, noShortcut, &QShortcut::deleteLater);
    connect(yesButton, &QPushButton::clicked, yesButton, &QPushButton::deleteLater);

    connect(box, &QMessageBox::finished, box, &QMessageBox::deleteLater);
    box->exec();

    showBlankUserDetails();

}
// Removes user from the database after we have accepted that we want to do that.
void MainWindow::onRemoveAccept()
{
    User* user = itemUserMap[selectedItem];

    userDatabase->removeUser(user);
}

