#include "user.h"

User::User()
{

}

QVariant User::toQVariant()
{
    QVariantMap map;

    map.insert("firstName", firstName_);
    map.insert("lastName", lastName_);
    map.insert("gender", gender_);
    map.insert("dateOfBirth", dateOfBirth_);


    QVariantMap homeNumberMap;
    homeNumberMap.insert("number", homeNumber_);
    homeNumberMap.insert("type", "home");

    QVariantMap mobileNumberMap;
    mobileNumberMap.insert("number", mobileNumber_);
    mobileNumberMap.insert("type", "mobile");

    QVariantList numberList;
    numberList.append(homeNumberMap);
    numberList.append(mobileNumberMap);

    map.insert("phoneNumbers", numberList);

    QVariantMap addressMap;
    addressMap.insert("city", city_);
    addressMap.insert("postalCode", postalCode_);
    addressMap.insert("state", state_);
    addressMap.insert("streetAddress", streetAddress_);

    map.insert("address", addressMap);

    return map;

}

void User::fromQVariant(const QVariant &variant)
{
    QVariantMap map = variant.toMap();

    firstName_ = map.value("firstName").toString();
    lastName_ = map.value("lastName").toString();
    gender_ = map.value("gender").toString();
    dateOfBirth_ = map.value("dateOfBirth").toString();

    QVariantMap addressMap = map.value("address").toMap();
    streetAddress_ = addressMap.value("streetAddress").toString();
    postalCode_ = addressMap.value("postalCode").toString();
    city_ = addressMap.value("city").toString();
    state_ = addressMap.value("state").toString();

    QVariantList numberList = map.value("phoneNumbers").toList();

    for (auto& x : numberList) {

        QString type = x.toMap().value("type").toString();
        QString number = x.toMap().value("number").toString();

        if (type == "home") {
            homeNumber_ = number;
        } else {
            mobileNumber_ = number;
        }
    }

}

QString User::toQString()
{
    return firstName_ + " " + lastName_;
}

