#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "userdatabase.h"
#include "userform.h"
#include <QListWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    UserDatabase* userDatabase;
    UserForm* userForm;
    std::map<QListWidgetItem*, User*> itemUserMap;
    QListWidgetItem* selectedItem = nullptr;


    void closeEvent(QCloseEvent *event);
    void load();
    void showBlankUserDetails();
    void addUserToListWidget(User* user);
    void removeUserFromListWidget();
    void showUserOnItem(QListWidgetItem *item);

private slots:
    void lwItemClicked(QListWidgetItem *item);
    void on_pbAdd_clicked();
    void on_pbRemove_clicked();
    void onRemoveAccept();
private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
