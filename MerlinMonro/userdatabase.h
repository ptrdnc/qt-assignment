#ifndef USERDATABASE_H
#define USERDATABASE_H

#include <QVector>
#include <QList>
#include "user.h"

class UserDatabase
{
public:
    UserDatabase(QWidget* parent);
    ~UserDatabase();
    QList<User*> users_;

    QString filePath_ = "../users.json";
    QWidget* parent_;

    void addUser(User* user);
    void removeUser(User* user);
    void loadUsers();
    void saveUsers();


};

#endif // USERDATABASE_H
