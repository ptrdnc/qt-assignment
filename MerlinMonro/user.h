#ifndef USER_H
#define USER_H

#include <QString>
#include <QPair>
#include <QVector>
#include <QVariant>


class User
{
public:
    User();

    QString firstName_;
    QString lastName_;
    QString gender_;
    QString dateOfBirth_;

    QString streetAddress_;
    QString postalCode_;
    QString city_;
    QString state_;

    QString homeNumber_;
    QString mobileNumber_;


    QVariant toQVariant();
    void fromQVariant(const QVariant &variant);

    QString toQString();

};

#endif // USER_H
