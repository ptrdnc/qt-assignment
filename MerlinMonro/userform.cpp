#include "userform.h"
#include "user.h"

#include "ui_userform.h"
#include <QShortcut>
#include <QValidator>

UserForm::UserForm(UserDatabase* userDatabase)
    : userDatabase_(userDatabase)
    , ui(new Ui::UserForm)
{
    ui->setupUi(this);

    setupShortcuts();
    setupValidators();
}

UserForm::~UserForm()
{
    delete ui;
    for (QValidator* x : validators) {
        delete x;
    }
    validators.clear();
    delete userDatabase_;
}
// resets focus. this had to be implemented because it was hard to unfocus listwidget
void UserForm::resetFocus()
{
    this->focusWidget()->clearFocus();
}
// clear all fields on the user form.
void UserForm::clearFields()
{
    ui->leFirstName->clear();
    ui->leLastName->clear();
    ui->cbGender->setCurrentIndex(0);
    ui->deDateOfBirth->setDate(QDate(1,1,2000));
    ui->leAddress->clear();
    ui->leCity->clear();
    ui->leState->clear();
    ui->lePostCode->clear();
    ui->leHomePhone->clear();
    ui->leMobilePhone->clear();

}
void UserForm::on_pbOk_clicked()
{

    if (ui->leFirstName->text() == "" || ui->leLastName->text() == "")
        return;


    User* user = new User();

    user->firstName_ = ui->leFirstName->text();
    user->lastName_ = ui->leLastName->text();
    user->gender_ = ui->cbGender->currentText();
    user->dateOfBirth_ = ui->deDateOfBirth->text();

    user->streetAddress_ = ui->leAddress->text();
    user->city_ = ui->leCity->text();
    user->state_ = ui->leState->text();
    user->postalCode_ = ui->lePostCode->text();
    user->homeNumber_ = ui->leHomePhone->text();
    user->mobileNumber_ = ui->leMobilePhone->text();

    userDatabase_->addUser(user);

    close();

}
void UserForm::on_pbCancel_clicked()
{
    close();
}
void UserForm::closeEvent(QCloseEvent *event)
{
    clearFields();
}
// sets up validators for each line edit
void UserForm::setupValidators()
{
    QRegularExpression nameRegex = QRegularExpression("^[a-zA-z'\\s]{1,}");

    firstNameValidator = new QRegularExpressionValidator(nameRegex, ui->leFirstName);
    ui->leFirstName->setValidator(firstNameValidator);
    lastNameValidator = new QRegularExpressionValidator(nameRegex, ui->leLastName);
    ui->leLastName->setValidator(lastNameValidator);

    QRegularExpression nameAndNumberRegex = QRegularExpression("[a-zA-z0-9\\s']+");

    streetAddressValidator = new QRegularExpressionValidator(nameAndNumberRegex, ui->leAddress);
    ui->leAddress->setValidator(streetAddressValidator);
    postalCodeValidator = new QIntValidator(0, 1000000, ui->lePostCode);
    ui->lePostCode->setValidator(postalCodeValidator);
    cityValidator = new QRegularExpressionValidator(nameRegex, ui->leCity);
    ui->leCity->setValidator(cityValidator);
    stateValidator = new QRegularExpressionValidator(nameRegex, ui->leState);
    ui->leState->setValidator(stateValidator);

    QRegularExpression numberRegex = QRegularExpression("^\\+?[0-9]+");
    homeNumberValidator = new QRegularExpressionValidator(numberRegex, ui->leHomePhone);
    ui->leHomePhone->setValidator(homeNumberValidator);
    mobileNumberValidator = new QRegularExpressionValidator(numberRegex, ui->leMobilePhone);
    ui->leMobilePhone->setValidator(mobileNumberValidator);

    validators = {firstNameValidator, lastNameValidator,
                  streetAddressValidator, postalCodeValidator, cityValidator, stateValidator,
                  homeNumberValidator, mobileNumberValidator};
}
// sets up keyboard shortcuts
void UserForm::setupShortcuts()
{
    QShortcut* cancelShortcut = new QShortcut(Qt::Key_C, this);
    connect(cancelShortcut, &QShortcut::activated, ui->pbCancel, &QPushButton::click);

    QShortcut* okShortcut = new QShortcut(Qt::Key_O, this);
    connect(okShortcut, &QShortcut::activated, ui->pbOk, &QPushButton::click);

    QShortcut* focusShortcut = new QShortcut(Qt::Key_Escape, this);
    connect(focusShortcut, &QShortcut::activated, this, &UserForm::resetFocus);
}
