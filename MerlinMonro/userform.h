#ifndef USERFORM_H
#define USERFORM_H

#include <QWidget>
#include <QValidator>
#include "userdatabase.h"
#include <QVector>

namespace Ui
{
class UserForm;
}

class UserForm : public QWidget
{
    Q_OBJECT
public:
    UserForm(UserDatabase* userDatabase);
    ~UserForm();
    UserDatabase* userDatabase_;

    void clearFields();
    void closeEvent(QCloseEvent *event);

    QVector<QValidator*> validators;

    QRegularExpressionValidator* firstNameValidator;
    QRegularExpressionValidator* lastNameValidator;
    QRegularExpressionValidator* streetAddressValidator;
    QIntValidator* postalCodeValidator;
    QRegularExpressionValidator* cityValidator;
    QRegularExpressionValidator* stateValidator;
    QRegularExpressionValidator* mobileNumberValidator;
    QRegularExpressionValidator* homeNumberValidator;

    /*
    QString firstName_;
    QString lastName_;
    QString gender_;
    QString dateOfBirth_;

    QString streetAddress_;
    QString postalCode_;
    QString city_;
    QString state_;

    QString homeNumber;
    QString mobileNumber;
*/


    void setupValidators();
    void setupShortcuts();

private slots:
    void on_pbOk_clicked();
    void on_pbCancel_clicked();
    void resetFocus();
private:
    Ui::UserForm* ui;
};

#endif // USERFORM_H
